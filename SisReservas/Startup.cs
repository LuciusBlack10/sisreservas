﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(SisReservas.Startup))]
namespace SisReservas
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
