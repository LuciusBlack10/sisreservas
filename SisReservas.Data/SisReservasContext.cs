﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SisReservas.Data
{
    /// <summary>
    /// Conexión con Base de datos
    /// </summary>
    public class SisReservasContext : DbContext
    {

        public SisReservasContext() : base("name=SisReservasContext")
        {
        }

        public System.Data.Entity.DbSet<SisReservas.Entities.Apartment> Apartments { get; set; }

        public System.Data.Entity.DbSet<SisReservas.Entities.City> Cities { get; set; }

        public System.Data.Entity.DbSet<SisReservas.Entities.Campus> Campus { get; set; }

        public System.Data.Entity.DbSet<SisReservas.Entities.Department> Departments { get; set; }

        public System.Data.Entity.DbSet<SisReservas.Entities.Country> Countries { get; set; }
    }

}
