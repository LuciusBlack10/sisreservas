﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SisReservas.Entities
{
    public class Reservation
    {
        [Key]
        public int IdReservation { get; set; }


        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM-dd}", ApplyFormatInEditMode = true)]
        [Display(Name = "Fecha Inicio")]
        public DateTime StartTime { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM-dd}", ApplyFormatInEditMode = true)]
        [Display(Name = "Fecha Fin")]
        public DateTime EndTime { get; set; }


        [Display(Name = "Noches")]
        [StringLength(2, ErrorMessage =
            "El campo {0} debe contener entre {1} y {2} caracteres ", MinimumLength = 1)]
        [Required(ErrorMessage = "{0} es obligatorio")]
        public int Night { get; set; }


        [Display(Name = "Personas")]
        [StringLength(2, ErrorMessage =
            "El campo {0} debe contener entre {1} y {2} caracteres ", MinimumLength = 1)]
        [Required(ErrorMessage = "{0} es obligatorio")]
        public int QuantityPeople { get; set; }

        [Display(Name = "Servicio de Lavanderia")]
        public bool Laundry { get; set; }



    }
}