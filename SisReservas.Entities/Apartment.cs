﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SisReservas.Entities
{
    public class Apartment
    {

        [Key]
        public int IdApartment { get; set; }

        public int IdCity { get; set; }
        public virtual City City { get; set; }


        public int IdRoom { get; set; }
        public virtual Room Room { get; set; }

        public decimal Price { get; set; }

    }
}