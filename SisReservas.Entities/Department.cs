﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SisReservas.Entities
{
    public class Department
    {
        [Key]
        public int IdDepartment { get; set; }

        [Display(Name = "Departamento")]
        [StringLength(30, ErrorMessage =
            "El campo {0} debe contener entre {1} y {2} caracteres ", MinimumLength = 3)]
        [Required(ErrorMessage = "{0} es obligatorio")]
        public string Name { get; set; } 
        public virtual ICollection<City>  Cities { get; set; }


        public int IdCountry { get; set; }
        [Display(Name="Pais")]
        public virtual Country Country { get; set; }
    }
}