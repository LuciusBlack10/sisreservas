﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SisReservas.Entities
{
    public class City
    {
        [Key]
        public int IdCity { get; set; }

        [Display(Name = "Nombre")] 
        [Required(ErrorMessage = "{0} es obligatorio")]
        [StringLength(30, ErrorMessage =
            "El campo {0} debe contener entre {1} y {2} caracteres ", MinimumLength = 2)]
        public string Name { get; set; }
           
        public int IdDepartment { get; set; }
        public virtual Department Department { get; set; }

        public virtual ICollection<Apartment> Apartments { get; set; }



    }
}