﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SisReservas.Entities
{
    public class Campus
    {
            
        [Key]
        public int IdCampus { get; set; }

        [Display(Name = "Nombre Sede")]
        [Required(ErrorMessage = "{0} es obligatorio")]
        [StringLength(20, ErrorMessage =
            "El campo {0} debe contener entre {1} y {2} caracteres ", MinimumLength = 2)]
        public string Name { get; set; }


        public int IdRoom { get; set; }
        public virtual Room Room { get; set; }


    }
}