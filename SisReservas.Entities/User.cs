﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SisReservas.Entities
{
    public class User
    {
        [Key]
        public int IdUser { get; set; }




        [Display(Name = "Tipo de documento")]
        public int DocumentTypeId { get; set; }
        [Display(Name = "Type de documento")]
        public virtual DocumentType DocumentType { get; set; }



        [Display(Name = "Documento")]
        [Required(ErrorMessage = "{0} es obligatorio")]
        [StringLength(20, ErrorMessage =
            "El campo {0} debe contener entre {1} y {2} caracteres ", MinimumLength = 7)]
        public string Document { get; set; }


        [Display(Name = "Nombre Completo")]
        [Required(ErrorMessage = "{0} es obligatorio")]
        [StringLength(30, ErrorMessage =
            "El campo {0} debe contener entre {1} y {2} caracteres ", MinimumLength = 3)]
        public string Name { get; set; }


        [Display(Name = "Fecha Nacimiento")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}",
                    ApplyFormatInEditMode = true)]
        public DateTime DateBhirthday { get; set; }


        [Display(Name = "Celular")]
        [Required(ErrorMessage = "{0} es obligatorio")]
        [StringLength(30, ErrorMessage =
         "El campo {0} debe contener entre {1} y {2} caracteres ", MinimumLength = 3)]
        public string CelPhone { get; set; }



        [Display(Name = "Email")]
        [StringLength(30, ErrorMessage =
            "El campo {0} debe contener entre {1} y {2} caracteres ", MinimumLength = 3)]
        [Required(ErrorMessage = "{0} es obligatorio")]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }



        public int IdDepartment { get; set; }
        public virtual Department Department { get; set; }


        public int IdCity { get; set; }
        public virtual City City { get; set; }




        [Display(Name = "Barrio")]

        [Required(ErrorMessage = "{0} es obligatorio")]
        public string Neighborhood { get; set; }



        [Display(Name = "Dirección")]

        [Required(ErrorMessage = "{0} es obligatorio")]
        public string Address { get; set; }



        [Display(Name = "Telefono Residencia")]
        [Required(ErrorMessage = "{0} es obligatorio")]
        [StringLength(30, ErrorMessage =
         "El campo {0} debe contener entre {1} y {2} caracteres ", MinimumLength = 3)]
        public string Phone { get; set; }


        public int IdSecretQuestion { get; set; }
        public virtual SecretQuestion SecretQuestion { get; set; }


        [Display(Name = "Respuesta Secreta")]
        [Required(ErrorMessage = "{0} es obligatorio")]
        [StringLength(30, ErrorMessage =
        "El campo {0} debe contener entre {1} y {2} caracteres ", MinimumLength = 3)]
        public string Answer { get; set; }


        public bool SendToCelphone { get; set; }
        public bool SendToEmail { get; set; }




        [Display(Name = "Contraseña")]

        [Required(ErrorMessage = "{0} es obligatorio")]
        [StringLength(4, ErrorMessage =
        "El campo {0} debe contener entre {1} y {2} caracteres ", MinimumLength = 4)]
        public string Password { get; set; }


        [Display(Name = "Comfirmar Contraseña")]
        [Required(ErrorMessage = "{0} es obligatorio")]
        [StringLength(4, ErrorMessage =
        "El campo {0} debe contener entre {1} y {2} caracteres ", MinimumLength = 4)]
        public string PasswordConfirm { get; set; }



    }
}