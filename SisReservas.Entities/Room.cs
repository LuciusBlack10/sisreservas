﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SisReservas.Entities
{
    public class Room
    {

        [Key]
        public int IdApartment { get; set; }

        public string Name { get; set; }
        public int Capacity { get; set; }


        public decimal Price { get; set; }

        public virtual ICollection<Apartment> Apartments { get; set; }

    }
}