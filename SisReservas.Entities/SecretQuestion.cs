﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SisReservas.Entities
{
    public class SecretQuestion
    {
        public int IdSecretQuestion { get; set; }
        public string Question { get; set; }
    }
}