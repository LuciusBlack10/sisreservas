﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SisReservas.Entities
{
    public class DocumentType
    {

        [Key] 
        public int DocumentTypeId { get; set; }

        [Display(Name = "Nombre")]
        [StringLength(20, ErrorMessage =
            "El campo {0} debe contener entre {1} y {2} caracteres ", MinimumLength = 2)]
        [Required(ErrorMessage = "{0} es obligatorio")]
        public string Name { get; set; }

        public virtual ICollection<User> Users { get; set; }

    }
}